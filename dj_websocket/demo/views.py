from django.http import HttpResponse
from django.shortcuts import render
from dwebsocket.decorators import accept_websocket, require_websocket


@require_websocket
def echo_once(request):
    message = request.websocket.wait()
    request.websocket.send(message)


@accept_websocket
def echo(request):
    if not request.is_websocket():  # 判断是不是websocket连接
        try:  # 如果是普通的http方法
            message = request.GET['message']
            return HttpResponse(message)
        except:
            return render(request, 'index.html')
    else:
        for message in request.websocket:
            try:
                request.websocket.send(message)  # 发送消息到客户端
            except:
                pass


def index(request):
    return render(request, 'index.html')


def index2(request):
    return render(request, 'index2.html')
